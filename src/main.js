import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import './assets/reset.css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'swiper/dist/css/swiper.css';
import 'video.js/dist/video-js.css'
Vue.config.productionTip = false;
import VideoPlayer  from 'vue-video-player';
Vue.use(VideoPlayer);
Vue.use(ElementUI);

// import echarts from 'echarts';
// Vue.prototype.$echarts = echarts;
//Vue.use(echarts);

import axios from 'axios'  // 安装axios后引入
Vue.prototype.$axios = axios  // 将axios挂载到原型上方便使用


new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
