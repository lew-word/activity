import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    list: []
  },
  getters: {
    list(state) {
      return state.list
    }
  },
  mutations: {
    setList(state, data) {
      state.list = data || []
    }
  },
  actions: {
    setListData({ commit }, params, vm) {
      vm.$axios.get('/api/admin/index/comments?current=1&size=10&region=1').then((res) => {
        console.log(res, 'lklk')
      })
    }
  },
  modules: {},
});
