const path = require('path');
module.exports = {
	// publicPath: process.env.NODE_ENV === 'pre' ? '/' : './',
	outputDir: 'dist', // 构建输出目录
	// assetsDir: 'servicecenter', // 静态资源目录 (js, css, img, fonts)
	lintOnSave: false, // 是否开启eslint保存检测，有效值：ture | false | 'error'
	runtimeCompiler: true, // 运行时版本是否需要编译
	transpileDependencies: [], // 默认babel-loader忽略mode_modules，这里可增加例外的依赖包名
	productionSourceMap: false, // 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度
	css: {
		// 配置高于chainWebpack中关于css loader的配置
		// modules: true, // 是否开启支持‘foo.module.css’样式
		// extract: true, // 是否使用css分离插件 ExtractTextPlugin，采用独立样式文件载入，不采用<style>方式内联至html文件中
		// sourceMap: false, // 是否在构建样式地图，false将提高构建速度
		loaderOptions: {
			// css预设器配置项
			less: {
				loaderOptions: {}
			}
		}
	},
	parallel: require('os').cpus().length > 1, // 构建时开启多进程处理babel编译
	pluginOptions: {
		'style-resources-loader': {
			preProcessor: 'less',
			patterns: [path.resolve(__dirname, './src/assets/less/global.less')]
		}
	},
	pwa: {
		iconPaths: {
			favicon32: 'favicon.ico',
			favicon16: 'favicon.ico',
			appleTouchIcon: 'favicon.ico',
			maskIcon: 'favicon.ico',
			msTileImage: 'favicon.ico'
		}
	},
	configureWebpack: {
		devtool: process.env.NODE_ENV === 'development' ? 'eval-module-cheap-source-map' : 'hidden-source-map',
		resolve: {
			alias: {
				'@': 'src/'
			}
		}
	},
	devServer: {
		overlay: {
			// 让浏览器 overlay 同时显示警告和错误
			warnings: true,
			errors: true
		},
		open: false, // 是否打开浏览器
		host: '0.0.0.0',
		port: '8080', // 代理断就
		hotOnly: false, // 热更新
		proxy: {
			'/api': {
				target: ' http://114.67.96.75:3040', // 目标代理接口地址
				secure: false,
				changeOrigin: true, // 开启代理，在本地创建一个虚拟服务端
				// ws: true, // 是否启用websockets
				pathRewrite: {
					'^/api': ''
				}
			}
		}
	}
};